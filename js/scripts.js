jQuery(document).ready(function($){
  // Make the menu responsive using slicknav
  $('#menu-main-menu').slicknav();

  // Run the bxSlider on testimonials
  $('.testimonials-list').bxSlider({
    controls: false,
    mode: 'fade'
  });

  // Create a fixed menu when the page reload
  // And the page position is >300
  const headerScroll = document.querySelector('.navigation-bar');
  const rect = headerScroll.getBoundingClientRect();
  const topDistance = Math.abs(rect.top);
  fixedMenu(topDistance);



});



window.onscroll = () => {
  const scroll = window.scrollY;
  fixedMenu(scroll);
}

// Create a fixed menu
function fixedMenu(scroll) {
  const headerScroll = document.querySelector('.navigation-bar');
  const documentBody = document.querySelector('body');

  // If the scroll >300 add classes
  if (scroll > 300) {
    headerScroll.classList.add('fixed-top');
  } else {
    headerScroll.classList.remove('fixed-top');
  }
}
