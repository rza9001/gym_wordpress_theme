<aside class="sidebar">
  <!-- Show the widget zone -->
  <?php
    if( is_active_sidebar('sidebar')):
      dynamic_sidebar('sidebar');  // sidebar = sidebar id from functions.php
    endif;  
  ?>
</aside>